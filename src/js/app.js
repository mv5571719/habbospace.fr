import Vue from 'vue'

import EnglishComponent from './components/EnglishComponent'
import FrenchComponent from './components/FrenchComponent'
import GermanComponent from './components/GermanComponent'

new Vue({
    el: '#app',

    data: {
        language: null
    },

    methods: {
        checked(language) {
            if (this.language === language) {
                return true
            }

            return false
        },

        change(language) {
            this.language = language
            this.setLanguage(language)
            this.hash(language)
        },

        hash(language) {
            if (document.location.hash) {
                let url = document.location.toString()
                document.location = url.split('#')[0] + "#" + language
            } else {
                document.location += "#" + language
            }
        },

        setLanguage(value) {
            let d = new Date();
            d.setTime(d.getTime() + (30*24*60*60*1000));
            
            document.cookie = "language=" + value + ";expires=" + d.toUTCString()
        }
    },

    components: {
        'french-body': FrenchComponent,
        'english-body': EnglishComponent,
        'german-body': GermanComponent
    },

    mounted() {
        if (document.location.hash) {
            this.language = document.location.hash.substr(1)
        } else {
            let cookie = document.cookie
        
            if (cookie !== "") {
                this.language = cookie.split('=')[1]
            } else {
                this.language = 'fr'
            }
        }

        this.hash(this.language)
    }
})