const path = require('path')
const VueLoaderPlugin = require('vue-loader/lib/plugin')


module.exports = {
    entry: './src/js/app.js',

    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'app.js'
    },

    mode: process.env.NODE_ENV,

    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.esm.js'
        },

        extensions: ['*', '.js', '.vue', '.json']
    },

    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            }
        ]
    },

    plugins: [
        new VueLoaderPlugin()
    ]

}