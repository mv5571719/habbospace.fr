FROM php:7.3-fpm

ADD . /var/www/html
WORKDIR /var/www/html

RUN chmod +x launch.sh

EXPOSE 9000
CMD ["./launch.sh"]
